
import { v4 as uuidv4 } from "uuid";
import { GetSimillarKeys, GetValueByKey } from "./helper-functions.js";
 export default class Storage{
    
    constructor(){
        this.StorageBox = [];
    };

        create(item){
        item.id = uuidv4();
        this.StorageBox.push(item);
        return item;
     };

        find(findFunc){
             return this.StorageBox.filter((storageItem) => {
                 findFunc(storageItem);  
            });
        };

        remove(findFunc){
            return this.StorageBox.filter((storageItem) => {
                !findFunc(storageItem).pop();
            });
        };
    
        getAll() {
         return this.StorageBox;
    };
        where(whereObj){
            return this.StorageBox.filter((items,index) => {
                GetSimillarKeys(item,whereObj).forEach((value) => {
                GetValueByKey(value, whereObj) == GetValueByKey(value,items);
                });
            });
    };
};
